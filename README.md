# InfInd Maze Resources

## How to install


```sh
# Go to the ros2 workspace (in the subject ~/project2_ws)
# Clone the repository
git clone https://gitlab.com/urjc-informatica-industrial/mazes_infind.git src/mazes_infind
colcon build --packages-select infind_maze_resources maze_collision_detector
```

## Launch simulator

```
ros2 launch infind_maze_resources infind_maze.launch.py
```