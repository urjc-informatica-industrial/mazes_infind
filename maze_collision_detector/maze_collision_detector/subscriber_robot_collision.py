import os
import rclpy
from rclpy.node import Node

from nav_msgs.msg import Odometry
from std_srvs.srv import Empty
from geometry_msgs.msg import Twist

ROBOT_COLLISION_MAX_Z = float(os.getenv('ROBOT_COLLISION_MAX_Z', '0.015'))

class RobotCollisionSubscriber(Node):

    def __init__(self):
        super().__init__('robot_collision_subscriber')
        self.subscription = self.create_subscription(
            Odometry,
            'odom',
            self.listener_callback,
            10)
        self.subscription  # prevent unused variable warning
        self.reset_cli = self.create_client(Empty, '/reset_world')
        self.pub_cmd_vel = self.create_publisher(Twist, '/cmd_vel', 10)
        while not self.reset_cli.wait_for_service(timeout_sec=1.0):
            self.get_logger().info('service not available, waiting again...')
        

    def listener_callback(self, msg: Odometry):        
        if msg.pose.pose.position.z > ROBOT_COLLISION_MAX_Z:
            self.get_logger().info('Reset robot on collision: "%s"' % msg.pose.pose.position.z)
            msg = Twist()
            self.pub_cmd_vel.publish(msg)
            req = Empty.Request()
            self.reset_cli.call_async(req)
            


def main(args=None):
    rclpy.init(args=args)

    robot_collision_subscriber = RobotCollisionSubscriber()

    rclpy.spin(robot_collision_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    robot_collision_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()