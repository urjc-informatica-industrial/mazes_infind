import os
import rclpy
import math
from rclpy.node import Node
from rclpy import qos
from datetime import datetime

from nav_msgs.msg import Odometry
from std_srvs.srv import Empty
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from rosgraph_msgs.msg import Clock
from builtin_interfaces.msg import Time

try:
    import pandas as pd
    USE_PANDAS=True
except ImportError:
    USE_PANDAS=False

class MazeRobotChecker(Node):

    def __init__(self):
        self._keep_running = True
        if USE_PANDAS:
            self.df = pd.DataFrame()

        self.clock : Time = None

        super().__init__('maze_robot_checker')
        self.lidar_sub = self.create_subscription(
            LaserScan,
            '/scan',
            self.lidar_status_callback,
            10)
        
        self.clock_sub = self.create_subscription(
            Clock,
            '/clock',
            self.update_clock,
            qos_profile=qos.qos_profile_system_default
        )

        self.bot_position = self.create_subscription(
            Odometry,
            '/odom',
            self.update_robot_position,
            qos_profile=qos.qos_profile_system_default
        )

    @property
    def keep_running(self):
        return self._keep_running

    def update_clock(self, msg: Clock):
        self.clock = msg.clock
        if self.clock.sec > 600:
            self.get_logger().info('Max time overdue. Finish')
            self.finish()

    def lidar_status_callback(self, msg: LaserScan):
        # self.get_logger().info('Front value: "%.4f"' % msg.ranges[0])
        # Check if range with 180 values
        if self.clock:
            count_inf = 0
            for i in range(0, 360):
                if msg.ranges[(180+i)%360] == math.inf:
                    count_inf += 1
                else:
                    count_inf = 0
                
                if count_inf >= 180:
                    self.get_logger().info(f'Got out of the maze in {self.clock.sec} seconds')
                    minutes = math.floor(self.clock.sec / 60)
                    seconds = self.clock.sec % 60
                    self.get_logger().info(f'Time in: {minutes} minutes and {seconds} seconds')                    
                    self.finish()
                    return
    
    def update_robot_position(self, msg: Odometry):
        if USE_PANDAS and self.clock:
            # self.get_logger().info('Update position')
            position = msg.pose.pose.position
            metric = {
                'time': datetime.now(),
                'rostime': self.clock.sec,
                'x': position.x,
                'y': position.y,
            }
            self.df = self.df.append(metric, ignore_index=True)        

    def finish(self):
        self.destroy_subscription(self.lidar_sub)
        self.destroy_subscription(self.clock_sub)
        self.destroy_subscription(self.bot_position)
        if USE_PANDAS:
            path=os.getenv('DF_STORE_PATH', '~/debug.pickle')
            self.df.to_pickle(path)
            print('Save pandas dataframe')
        self._keep_running = False

def main(args=None):
    rclpy.init(args=args)

    robot_collision_subscriber = MazeRobotChecker()

    while robot_collision_subscriber.keep_running:
        rclpy.spin_once(robot_collision_subscriber)

    # Destroy the node explicitly
    # (optional - otherwise it will be done automatically
    # when the garbage collector destroys the node object)
    robot_collision_subscriber.destroy_node()
    rclpy.shutdown()


if __name__ == '__main__':
    main()
