from setuptools import setup

package_name = 'maze_evaluator'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='diegogd',
    maintainer_email='diego.gdiaz@urjc.es',
    description='TODO: Package description',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'maze_robot_checker = maze_evaluator.maze_robot_checker:main'
        ],
    },
)
